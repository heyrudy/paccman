/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data.tools.info;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.HelpCtx;

@ActionID(category = "File",
id = "com.moonstice.paccman.data.tools.info.ShowPropertiesAction")
@ActionRegistration(iconBase = "com/moonstice/paccman/data/tools/info/info_16x16.png",
displayName = "#CTL_ShowPropertiesAction")
@ActionReferences({
    @ActionReference(path = "Menu/File", position = 2550, separatorAfter = 2575)
})
public final class ShowPropertiesAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        DataPropertyPanel propPanel = new DataPropertyPanel();
        DialogDescriptor dd = new DialogDescriptor(
            propPanel,
            "Properties",
            true,
            new Object[] { "Close" },
            null, DialogDescriptor.DEFAULT_ALIGN, HelpCtx.DEFAULT_HELP, null);
        DialogDisplayer.getDefault().notify(dd);
     }

}
