/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class test for backspace key.
 *
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcBackspaceTest extends CalcParserTestBase {

    public CalcBackspaceTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    /**
     * Test recognition of backspace key.
     */
    @Test
    public void testBackspaceToken() {
        System.out.println("BackspaceToken");

        testStringParse("b", "0");
    }

    /**
     * Test backspace when entering integer.
     */
    @Test
    public void testBackspaceWhenEnteringInteger() {
        System.out.println("BackspaceWhenEnteringInteger");

        testStringParse("1b23=", "1", "0", "2", "23", "23");
        testStringParse("12b3=", "1", "12", "1", "13", "13");
        testStringParse("123b=", "1", "12", "123", "12", "12");
    }

    /**
     * Test backspace when entering decimal part.
     */
    @Test
    public void testBackspaceWhenEnteringDecimal() {
        System.out.println("BackspaceWhenEnteringDecimal");

        testStringParse("1.2b34=", "1", "1.", "1.2", "1.", "1.3", "1.34", "1.34");
        testStringParse("1.23b4=", "1", "1.", "1.2", "1.23", "1.2", "1.24", "1.24");
        testStringParse("1.234b=", "1", "1.", "1.2", "1.23", "1.234", "1.23", "1.23");
    }

    /**
     * Test backspace as first key.
     */
    @Test
    public void testBackspaceFirst() {
        System.out.println("BackspaceFirst");

        testStringParse("b123=", "0", "1", "12", "123", "123");
    }

    /**
     * Test backsapce after decimal point.
     */
    @Test
    public void testBackspaceAfterDecimalPoint() {
        System.out.println("BackspaceAfterDecimalPoint");

        testStringParse("1.b23=", "1", "1.", "1", "12", "123", "123");
    }

    /**
     * Test multiple backspaces.
     */
    @Test
    public void testMultipleBackspaces() {
        System.out.println("MultipleBackspaces");

        testStringParse("1b234b=", "1", "0", "2", "23", "234", "23", "23");
        testStringParse("1bb234b=", "1", "0", "0", "2", "23", "234", "23", "23");
        testStringParse("12bb345b=", "1", "12", "1", "0", "3", "34", "345", "34", "34");
        testStringParse("123bb456b=", "1", "12", "123", "12", "1", "14", "145", "1456", "145", "145");

        testStringParse("1.234bb5=", "1", "1.", "1.2", "1.23", "1.234", "1.23",
                "1.2", "1.25", "1.25");
        testStringParse("1.23bb=", "1", "1.", "1.2", "1.23", "1.2", "1.", "1");
        testStringParse("12.34bbbb=", "1", "12", "12.", "12.3", "12.34", "12.3",
                "12.", "12", "1", "1");

        testStringParse("6+23bbb=", "6", "6", "2", "23", "2", "0", "0", "6");
        testStringParse("23bbb+6=", "2", "23", "2", "0", "0", "0", "6", "6");
    }

    @Test
    public void testMisplacedBackspace() {
        System.out.println("MisplacedBackspace");

        testStringParse("=b", "0", false);
        testStringParse("1+5%b=", "1", "1", "5", "0.05", false, "1.05");
        testStringParse("(1+5)b=", "0", "1", "1", "5", "6", false, "6");
        testStringParse("2+b5=", "2", "2", false, "5", "7");
        testStringParse("2-b5=", "2", "2", false, "5", "-3");
        testStringParse("2xb5=", "2", "2", false, "5", "10");
        testStringParse("2/b5=", "2", "2", false, "5", "0.4");
        testStringParse("(b1+5)=", "0", false, "1", "1", "5", "6", "6");
    }

    @Test
    public void testBackspaceAndCe() {
        System.out.println("BackspaceAndCe");

        testStringParse("12cb=", "1", "12", "0", "0", "0");
        testStringParse("12bc=", "1", "12", "1", "0", "0");
        testStringParse("1+2=cb45=", "1", "1", "2", "3", "0", "0", "4", "45", "45");
        testStringParse("1+2=bc45=", "1", "1", "2", "3", false, "0", "4", "45", "45");
        testStringParse("1+2=b4c5=", "1", "1", "2", "3", false, "4", "0", "5", "5");
    }

    @Test
    public void testBackspaceAfterReset() {
        System.out.println("BackspaceAfterReset");

        testStringParse("12zb", "1", "12", "0", "0");
    }
}
