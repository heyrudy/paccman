/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcFatalErrorTest extends CalcParserTestBase {

    public CalcFatalErrorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testDivByZero() {
        System.out.println("DivByZero");

        testStringParse("1/0=", "1", "1", "0", ExprParser.FatalError.DIV_BY_ZERO);
        testStringParse("1/0.0=", "1", "1", "0", "0.", "0.0", ExprParser.FatalError.DIV_BY_ZERO);
        testStringParse(3, "1/0.01=", "1", "1", "0", "0.", "0.0", false, ExprParser.FatalError.DIV_BY_ZERO);
        testStringParse(3, "1/0/0=", "1", "1", "0", ExprParser.FatalError.DIV_BY_ZERO, false, false);
    }

    @Test
    public void testOverflow() {
        System.out.println("Overflow");

        testStringParse(3, "999+1=", "9", "99", "999", "999", "1", ExprParser.FatalError.OVERFLOW);
        testStringParse(3, "999x1x2=", "9", "99", "999", "999", "1", "999", "2", ExprParser.FatalError.OVERFLOW);
    }

    @Test
    public void testFatalRecoveryOnlyOnReset() {
        System.out.println("FatalRecoveryOnlyOnReset");
        
        testStringParse("1/0=1+z12=", "1", "1", "0", ExprParser.FatalError.DIV_BY_ZERO,
                false, false, "0", "1", "12", "12");
        testStringParse(3, "999x1x2=45x+z1s2=3.4=", "9", "99", "999", "999", "1", "999", "2", ExprParser.FatalError.OVERFLOW,
                false, false, false, false, "0", "1", "-1", "-12", "-12", "3", "3.", "3.4", "3.4");
    }

    @Test
    public void testFatalErrorOnPriorityOperator()
    {
        System.out.println("FatalErrorOnPriorityOperator");
        
        testStringParse("+963bc6/0=", "0", "9", "96", "963", "96", "0", "6", "6", "0", ExprParser.FatalError.DIV_BY_ZERO);
    }

}
