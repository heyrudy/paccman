/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcResetTest extends CalcParserTestBase {

    public CalcResetTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    /**
     * Test ignore all keys until RESET
     */
    @Test
    public void testKeyIgnoredUntilReset() {
        System.out.println("KeyIgnoredUntilReset");

        testStringParse("1/0/0123456789c+-x/z.2=", "1", "1", "0", ExprParser.FatalError.DIV_BY_ZERO,
                false, false, false, false, false, false, false, false, false, false, false,
                false, false, false, false, "0", "0.", "0.2", "0.2");
    }

    /**
     * Test operation after RESET
     */
    @Test
    public void testOperationAfterReset() {
        System.out.println("OperationAfterReset");

        testStringParse("1/0=12c3z12+32=25-6=", "1", "1", "0", ExprParser.FatalError.DIV_BY_ZERO,
                false, false, false, false, "0", "1", "12", "12", "3", "32", "44", "2", "25", "25",
                "6", "19");
    }

}
