/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import com.moonstice.paccman.calculator.impl.parser.ExprParser.FatalError;
import org.testng.Assert;

/**
 * Base class for parser class tests.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcParserTestBase {

    public static final int DEFAULT_MAX_PRECISION = 20;

    private FatalError lastFatalError;

    /**
     * The expected result may be: <ul>
     * <li>a string: the key is valid and the string is the value that should be displayed
     * <li>a boolean: the key is invalid
     * <li>a FatalError: the key triggerred a fatal error ({@code CalcParser.parseChar} should return {@code false})
     * </ul>
     * @param maxPrecision
     * @param input
     * @param exp
     */
    protected void testStringParse(int maxPrecision, String input, Object... exp) {
        assert (exp.length == input.length()) : "Input length must match number of expected displays";
        String lastValidDisplay = "0";
        CalcParser instance = new CalcParser(maxPrecision);
        for (int i = 0; i < input.length(); i++) {
            final char c = input.charAt(i);
            if (exp[i] == null) {
                Assert.fail("Expected input can not be null");
            }

            final boolean parseCharResult = instance.parseChar(c);

            if (exp[i] instanceof String) {
                Assert.assertTrue(parseCharResult);
                Assert.assertEquals(exp[i], instance.getDisplay());
                lastValidDisplay = (String) (exp[i]);
            } else if (exp[i] instanceof Boolean) {
                Assert.assertEquals(exp[i], parseCharResult);
                if (instance.exprParser.getFatalError() == null) {
                    Assert.assertEquals(lastValidDisplay, instance.getDisplay());
                } else {
                    Assert.assertEquals(lastFatalError, instance.exprParser.fatalError);
                }
            } else if (exp[i] instanceof ExprParser.FatalError) {
                Assert.assertFalse(parseCharResult);
                Assert.assertEquals((ExprParser.FatalError) (exp[i]), instance.exprParser.fatalError);
                lastFatalError = instance.exprParser.fatalError;
            } else {
                Assert.fail("Invalid expected input in test");
            }
        }
    }

    protected void testStringParse(String input, Object... exp) {
        testStringParse(DEFAULT_MAX_PRECISION, input, exp);
    }

}
