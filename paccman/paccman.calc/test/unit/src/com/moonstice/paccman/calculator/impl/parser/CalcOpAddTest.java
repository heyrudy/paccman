/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class for testing add operation.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcOpAddTest extends CalcParserTestBase {

    public CalcOpAddTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testAddTwoOperands() {
        System.out.println("AddTwoOperands");

        testStringParse("1+2=", "1", "1", "2", "3");
        testStringParse("12+3=", "1", "12", "12", "3", "15");
        testStringParse("1+34=", "1", "1", "3", "34", "35");
        testStringParse("12+45=", "1", "12", "12", "4", "45", "57");
        testStringParse("1.2+34=", "1", "1.", "1.2", "1.2", "3", "34", "35.2");
        testStringParse("1+3.4=", "1", "1", "3", "3.", "3.4", "4.4");
        testStringParse("1.2+3.4=", "1", "1.", "1.2", "1.2", "3", "3.", "3.4", "4.6");
    }

    @Test
    public void testAddThreeOperands() {
        System.out.println("AddThreeOperands");

        testStringParse("1+3+7=", "1", "1", "3", "4", "7", "11");
        testStringParse("1+23+5.7=", "1", "1", "2", "23", "24", "5", "5.", "5.7", "29.7");
        testStringParse("1+2s3+5.7=", "1", "1", "2", "-2", "-23", "-22", "5", "5.", "5.7", "-16.3");
        testStringParse("1+23+5.7s=", "1", "1", "2", "23", "24", "5", "5.", "5.7", "-5.7", "18.3");
        testStringParse("1+023+5.7s=", "1", "1", "0", "2", "23", "24", "5", "5.", "5.7", "-5.7", "18.3");
        testStringParse("1+023+5.70=", "1", "1", "0", "2", "23", "24", "5", "5.", "5.7", "5.70", "29.7");
    }

    @Test
    public void testAddMoreThanThreeOperands() {
        System.out.println("AddMoreThanThreeOperands");

        testStringParse("1+3+7+15+27+49=", "1", "1", "3", "4", "7", "11",
                "1", "15", "26", "2", "27", "53", "4", "49", "102");
        testStringParse("1+3+7+15+27+4s9=", "1", "1", "3", "4", "7", "11",
                "1", "15", "26", "2", "27", "53", "4", "-4", "-49", "4");
    }

    @Test
    public void testAddWithOperandWithMaxDigits() {
        System.out.println("AddWithOperandWithMaxDigits");

        testStringParse(3, "12+3456=", "1", "12", "12", "3", "34", "345", false, "357");
        testStringParse(3, "1234+56=", "1", "12", "123", false, "123", "5", "56", "179");
    }

}
