/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import java.math.BigDecimal;
import com.moonstice.paccman.calculator.impl.parser.ExprParser.FatalError;
import org.testng.Assert;
import org.testng.annotations.*;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcRoundTest extends CalcParserTestBase {

    public CalcRoundTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testNoRounding() {
        System.out.println("NoRounding");

        testRoundedTo(6, "0", "0");
        testRoundedTo(6, "123", "123");
        testRoundedTo(6, "12.3", "12.3");
        testRoundedTo(6, "100", "100");
        testRoundedTo(6, "0.2", "0.2");
        testRoundedTo(6, "0.02", "0.02");
        testRoundedTo(6, "20.02", "20.02");
    }

    @Test
    public void testRemoveTrailingZeros() {
        System.out.println("RemoveTrailingZeros");

        testRoundedTo(6, "0.0", "0");
        testRoundedTo(6, "123.0", "123");
        testRoundedTo(6, "12.30", "12.3");
        testRoundedTo(6, "100.0", "100");
        testRoundedTo(6, "0.20", "0.2");
        testRoundedTo(6, "0.020", "0.02");
    }

    @Test
    public void testRoundingDecimalDown() {
        System.out.println("RoundingDecimalDown");

        testRoundedTo(3, "1.23", "1.2");
        testRoundedTo(3, "1.239999999", "1.2");
        testRoundedTo(5, "1.12345", "1.123");
        testRoundedTo(3, "0.123", "0.1");
        testRoundedTo(3, "1.01", "1");
        testRoundedTo(3, "0.01", "0");
        testRoundedTo(3, "0.0101", "0");
    }

    @Test
    public void testRoundingDecimalUp() {
        System.out.println("RoundingDecimalUp");

        testRoundedTo(3, "1.26", "1.3");
        testRoundedTo(3, "1.259999999", "1.3");
        testRoundedTo(5, "1.1236", "1.124");
        testRoundedTo(3, "0.163", "0.2");
        testRoundedTo(3, "0.06", "0.1");
    }

    @Test
    public void testRoudingToInteger() {
        System.out.println("RoudingToInteger");

        testRoundedTo(3, "123.1", "123");
        testRoundedTo(3, "123.6", "124");
    }

    @Test
    public void testOverflow() {
        System.out.println("Overflow");

        testRoundedTo(3, "999", "999");
        testRoundedTo(3, "1000", null, ExprParser.FatalError.OVERFLOW);
        testRoundedTo(3, "999.2", "999");
        testRoundedTo(3, "1000.2", null, ExprParser.FatalError.OVERFLOW);
        testRoundedTo(3, "999.6", null, ExprParser.FatalError.OVERFLOW);
    }

    @Test
    public void testNoRoundingWithSign() {
        System.out.println("NoRoundingWithSign");

        testRoundedTo(6, "-0", "0");
        testRoundedTo(6, "-123", "-123");
        testRoundedTo(6, "-12.3", "-12.3");
        testRoundedTo(6, "-100", "-100");
        testRoundedTo(6, "-0.2", "-0.2");
        testRoundedTo(6, "-0.02", "-0.02");
    }

    @Test
    public void testRemoveTrailingZerosWithSign() {
        System.out.println("RemoveTrailingZerosWithSign");

        testRoundedTo(6, "-0.0", "0");
        testRoundedTo(6, "-123.0", "-123");
        testRoundedTo(6, "-12.30", "-12.3");
        testRoundedTo(6, "-100.0", "-100");
        testRoundedTo(6, "-0.20", "-0.2");
        testRoundedTo(6, "-0.020", "-0.02");
    }

    @Test
    public void testRoundingDecimalDownWithSign() {
        System.out.println("RoundingDecimalDownWithSign");

        testRoundedTo(3, "-1.23", "-1.2");
        testRoundedTo(3, "-1.239999999", "-1.2");
        testRoundedTo(5, "-1.12345", "-1.123");
        testRoundedTo(3, "-0.123", "-0.1");
        testRoundedTo(3, "-1.01", "-1");
        testRoundedTo(3, "-0.01", "0");
        testRoundedTo(3, "-0.0101", "0");
    }

    @Test
    public void testRoundingDecimalUpWithSign() {
        System.out.println("RoundingDecimalUpWithSign");

        testRoundedTo(3, "-1.26", "-1.3");
        testRoundedTo(3, "-1.259999999", "-1.3");
        testRoundedTo(5, "-1.1236", "-1.124");
        testRoundedTo(3, "-0.163", "-0.2");
        testRoundedTo(3, "-0.06", "-0.1");
    }

    @Test
    public void testRoudingToIntegerWithSign() {
        System.out.println("RoudingToIntegerWithSign");

        testRoundedTo(3, "-123.1", "-123");
        testRoundedTo(3, "-123.6", "-124");
    }

    @Test
    public void testOverflowWithSign() {
        System.out.println("OverflowWithSign");

        testRoundedTo(3, "-999", "-999");
        testRoundedTo(3, "-1000", null, ExprParser.FatalError.OVERFLOW);
        testRoundedTo(3, "-999.2", "-999");
        testRoundedTo(3, "-1000.2", null, ExprParser.FatalError.OVERFLOW);
        testRoundedTo(3, "-999.6", null, ExprParser.FatalError.OVERFLOW);
    }

    @Test
    public void testUseRoundedValueAsOperand() {
        System.out.println("UseRoundedValueAsOperand");

        testStringParse(4, "2/3x2=", "2", "2", "3", "0.67", "2", "1.34");
    }

    private void testRoundedTo(final int precision, final String result, final String expected) {
        testRoundedTo(precision, result, expected, null);
    }

    private void testRoundedTo(final int precision, final String result, final String expected, final FatalError fatalError) {
        ExprParser parser = new ExprParser(precision);
        final BigDecimal roundResult = parser.round(new BigDecimal(result), precision);

        if (fatalError != null) {
            Assert.assertNull(roundResult);
            Assert.assertTrue(parser.fatalError == fatalError);
        } else {
            Assert.assertEquals(new BigDecimal(expected), roundResult);
        }
    }

}
