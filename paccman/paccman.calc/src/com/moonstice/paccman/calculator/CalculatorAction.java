/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import com.moonstice.paccman.calculator.impl.ui.CalculatorPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

@ActionID(category = "Tools",
id = "com.moonstice.paccman.calculator.CalculatorAction")
@ActionRegistration(iconBase = "com/moonstice/paccman/calculator/calc_16x16.png",
displayName = "#CTL_CalculatorAction")
@ActionReferences({
    @ActionReference(path = "Menu/Tools", position = 600, separatorAfter = 800)
})
public final class CalculatorAction implements ActionListener {

    JDialog calculatorDlg;

    @Override
    public void actionPerformed(ActionEvent e) {
        if (calculatorDlg == null) {
            final CalculatorPanel cp = new CalculatorPanel();
            DialogDescriptor dd = new DialogDescriptor(
                    cp, NbBundle.getMessage(CalculatorAction.class, "CalculatorDialog.title"), false,
                    new Object[0], null, 0, HelpCtx.DEFAULT_HELP, this);
            calculatorDlg = (JDialog) DialogDisplayer.getDefault().createDialog(dd);
            calculatorDlg.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            cp.setFocusable(true);
            calculatorDlg.setResizable(false);
            calculatorDlg.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosed(WindowEvent e) {
                    calculatorDlg = null;
                }

            });
        }
        calculatorDlg.setVisible(true);
    }

}
