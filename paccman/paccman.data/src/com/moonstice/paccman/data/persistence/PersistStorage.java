/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data.persistence;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
public interface PersistStorage {

    /**
     * Opens the persistence storage at the specified path.
     * @param path
     * @throws Exception
     */
    void open(String path) throws Exception;

    /**
     * Closes the persistence storage.
     * @throws Exception
     */
    void close() throws Exception;

    /**
     * Returns the path for storage.
     * @return the storage path.
     */
    String getStoragePath();

    /**
     * Backup the persistence storage to the specified location.
     * @param toPath the backup destination path.
     * @throws Exception 
     */
    public void backup(String toPath) throws Exception;

}
