/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data.persistence.impl;

import java.io.File;
import java.sql.Connection;
import com.moonstice.paccman.data.persistence.PersistStorage;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@ServiceProvider(service=PersistStorage.class)
public class PersistStorageImpl implements PersistStorage {

    @Override
    public void open(String path) throws Exception {
        if (databaseConnection != null) {
            throw new IllegalStateException("Already opened."); // NOI18N
        }
        databaseConnection = DerbyUtil.openDatabase(path);
        storagePath = path;
    }

    @Override
    public void close() throws Exception {
        if (databaseConnection == null) {
            return;
        }
        databaseConnection.close();
        databaseConnection = null;
        DerbyUtil.shutdownDatabase(storagePath);
        storagePath = null;
    }

    private Connection databaseConnection;

    private String storagePath;

    @Override
    public String getStoragePath() {
        return storagePath;
    }

    @Override
    public void backup(String toPath) throws Exception {
        File f = new File(toPath);
        String backupPath = f.getParentFile().toString();
        String dbBackupName = f.getName();
        DerbyUtil.backupDatabase(databaseConnection, backupPath, getDatabaseName(), dbBackupName);
    }

    private String getDatabaseName() {
        return new File(storagePath).getName();
    }

}
