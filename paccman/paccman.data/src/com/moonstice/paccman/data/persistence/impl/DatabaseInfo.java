/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data.persistence.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import com.moonstice.paccman.data.persistence.Persistable;
import org.openide.util.Exceptions;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "DATABASEINFOS")
@NamedQueries({
    @NamedQuery(name = "DatabaseInfo.findAll", query = "SELECT d FROM DatabaseInfo d"),
    @NamedQuery(name = "DatabaseInfo.findByInfoid", query = "SELECT d FROM DatabaseInfo d WHERE d.infoid = :infoid"),
    @NamedQuery(name = "DatabaseInfo.findByInfovalue", query = "SELECT d FROM DatabaseInfo d WHERE d.infovalue = :infovalue")})
public class DatabaseInfo implements Persistable {

    private static final long serialVersionUID = 1L;

    private static final SimpleDateFormat DBINFO_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @Id
    @Basic(optional = false)
    @Column(name = "INFOID")
    private String infoid;

    @Column(name = "INFOVALUE")
    private String infovalue;

    public DatabaseInfo() {
    }

    public DatabaseInfo(String infoid) {
        this.infoid = infoid;
    }

    public DatabaseInfo(String infoid, String value) {
        this.infoid = infoid;
        this.infovalue = value;
    }

    public DatabaseInfo(String infoid, Calendar date) {
        this(infoid, DBINFO_DATE_FORMAT.format(date.getTime())); //NOI18N
    }

    public String getInfoid() {
        return infoid;
    }

    public void setInfoid(String infoid) {
        this.infoid = infoid;
    }

    public String getInfovalue() {
        return infovalue;
    }

    public Calendar getCalendarInfoValue() {
        final Date date;
        try {
            date = DBINFO_DATE_FORMAT.parse(infovalue);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            return c;
        } catch (ParseException ex) {
            Exceptions.printStackTrace(ex);
            return null;
        }
    }

    public void setCalendarInfoValue(Calendar c) {
        infovalue = DBINFO_DATE_FORMAT.format(c.getTime());
    }

    public void setInfovalue(String infovalue) {
        this.infovalue = infovalue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (infoid != null ? infoid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatabaseInfo)) {
            return false;
        }
        DatabaseInfo other = (DatabaseInfo) object;
        if ((this.infoid == null && other.infoid != null) || (this.infoid != null && !this.infoid.equals(other.infoid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.persistence.DatabaseInfo[infoid=" + infoid + "]"; // NOI18N
    }

    public void setString(String id, String value) {
        setInfoid(id);
        setInfovalue(value);
    }

}
