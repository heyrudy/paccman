/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data.persistence.impl;

import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
public class DerbyUtil {

    private static final String DEFAULT_ENCODING = "utf-8"; // NOI18N

    private static String buildConnectionString(String dbPath) {
        return "jdbc:derby:" + dbPath; // NOI18N
    }

    static Connection openDatabase(String dbPath) throws SQLException {
        String cnxUrl = buildConnectionString(dbPath);
        return DriverManager.getConnection(cnxUrl);
    }

    static Connection createDatabase(String dbPath) throws SQLException {
        String cnxUrl = buildConnectionString(dbPath);
        return DriverManager.getConnection(cnxUrl + ";create=true"); // NOI18N
    }

    static void shutdownDatabase(String dbPath) throws SQLException {
        String cnxUrl = buildConnectionString(dbPath);
        try {
            DriverManager.getConnection(cnxUrl + ";shutdown=true"); // NOI18N
        } catch (SQLException se) {
            // Database shutdown throws the 08006 exception to confirm success.
            if (!se.getSQLState().equals("08006")) { // NOI18N
                throw se;
            }
        }
    }

    static void backupDatabase(Connection cnx, String backupDir, String dbName, String dbBackupName) throws SQLException {
        CallableStatement cs = cnx.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)"); // NOI18N
        cs.setString(1, backupDir);
        cs.execute();
        cs.close();
        FileObject fo = FileUtil.toFileObject(new File(backupDir + "/" + dbName)); // NOI18N
        FileLock lock = null;
        try {
            lock = fo.lock();
            fo.rename(lock, dbBackupName, ""); // NOI18N
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } catch (Exception e) {
            Exceptions.printStackTrace(e);
        } finally {
            lock.releaseLock();
        }

    }

    private DerbyUtil() {
    }
}
