/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJTRANSFERS")
@NamedQueries({
    @NamedQuery(name = "Transfer.findAll", query = "SELECT t FROM Transfer t"),
    @NamedQuery(name = "Transfer.findByIdtransactionentry", query = "SELECT t FROM Transfer t WHERE t.idtransactionentry = :idtransactionentry")})
public class Transfer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDTRANSACTIONENTRY")
    private Integer idtransactionentry;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transfer")
    private Collection<Transfer> transferCollection;

    @JoinColumn(name = "IDOTHERTRANSACTIONENTRY", referencedColumnName = "IDTRANSACTIONENTRY")
    @ManyToOne(optional = false)
    private Transfer transfer;

    @JoinColumn(name = "IDTRANSACTIONENTRY", referencedColumnName = "IDTRANSACTIONENTRY", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private TransactionEntry transactionEntry;

    public Transfer() {
    }

    public Transfer(Integer idtransactionentry) {
        this.idtransactionentry = idtransactionentry;
    }

    public Integer getIdtransactionentry() {
        return idtransactionentry;
    }

    public void setIdtransactionentry(Integer idtransactionentry) {
        this.idtransactionentry = idtransactionentry;
    }

    public Collection<Transfer> getTransferCollection() {
        return transferCollection;
    }

    public void setTransferCollection(Collection<Transfer> transferCollection) {
        this.transferCollection = transferCollection;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public TransactionEntry getTransactionEntry() {
        return transactionEntry;
    }

    public void setTransactionEntry(TransactionEntry transactionEntry) {
        this.transactionEntry = transactionEntry;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtransactionentry != null ? idtransactionentry.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transfer)) {
            return false;
        }
        Transfer other = (Transfer) object;
        if ((this.idtransactionentry == null && other.idtransactionentry != null) || (this.idtransactionentry != null && !this.idtransactionentry.equals(other.idtransactionentry))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.Transfer[idtransactionentry=" + idtransactionentry + "]";
    }

}
