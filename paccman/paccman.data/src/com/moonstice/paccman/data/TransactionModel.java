/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJTRANSACTIONMODELS")
@NamedQueries({
    @NamedQuery(name = "TransactionModel.findAll", query = "SELECT t FROM TransactionModel t"),
    @NamedQuery(name = "TransactionModel.findByIdtransactionmodel", query = "SELECT t FROM TransactionModel t WHERE t.idtransactionmodel = :idtransactionmodel"),
    @NamedQuery(name = "TransactionModel.findByTransactionmodelname", query = "SELECT t FROM TransactionModel t WHERE t.transactionmodelname = :transactionmodelname"),
    @NamedQuery(name = "TransactionModel.findByDescription", query = "SELECT t FROM TransactionModel t WHERE t.description = :description")})
public class TransactionModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDTRANSACTIONMODEL")
    private Integer idtransactionmodel;

    @Basic(optional = false)
    @Column(name = "TRANSACTIONMODELNAME")
    private String transactionmodelname;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToMany(mappedBy = "transactionModelCollection")
    private Collection<Category> categoryCollection;

    public TransactionModel() {
    }

    public TransactionModel(Integer idtransactionmodel) {
        this.idtransactionmodel = idtransactionmodel;
    }

    public TransactionModel(Integer idtransactionmodel, String transactionmodelname) {
        this.idtransactionmodel = idtransactionmodel;
        this.transactionmodelname = transactionmodelname;
    }

    public Integer getIdtransactionmodel() {
        return idtransactionmodel;
    }

    public void setIdtransactionmodel(Integer idtransactionmodel) {
        this.idtransactionmodel = idtransactionmodel;
    }

    public String getTransactionmodelname() {
        return transactionmodelname;
    }

    public void setTransactionmodelname(String transactionmodelname) {
        this.transactionmodelname = transactionmodelname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Category> getCategoryCollection() {
        return categoryCollection;
    }

    public void setCategoryCollection(Collection<Category> categoryCollection) {
        this.categoryCollection = categoryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtransactionmodel != null ? idtransactionmodel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionModel)) {
            return false;
        }
        TransactionModel other = (TransactionModel) object;
        if ((this.idtransactionmodel == null && other.idtransactionmodel != null) || (this.idtransactionmodel != null && !this.idtransactionmodel.equals(other.idtransactionmodel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.TransactionModel[idtransactionmodel=" + idtransactionmodel + "]";
    }

}
